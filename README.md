# S360 Tech Test - React (js-ui-react)
Hello! Thank you for taking the time to complete this test. Below, you will find instructions on how to complete the UI
portion of the tech test. Please follow the setup instructions to get the UI up and running. If you run into issues
getting the UI to work, please reach out to Clyde Nelson [cnelson@storage360.com](mailto:cnelsonl@storage360.com)
for further assistance.

## Goals
The goal of this portion of the test is to demonstrate your knowledge of working with JavaScript/TypeScript, React,
an HTTP client and a UI component library. It is also meant to test your ability to quickly learn the conventions of a
new codebase, follow those conventions for the sake of consistency, and test your ability to problem solve.

## Setup Instructions
This setup assumes that you have a development environment that has Node.js and npm installed, a terminal, and
knowledge on how to use the terminal. **It also assumes you have already set up the PHP-powered Laravel API.**

1. Fork this repository so that you have your own version in your GitLab account
   * Make sure that you mark your repository as public so that we can review it
2. Open your terminal and clone your fork (**DO NOT CLONE THE MAIN REPOSITORY!**)
3. `$ npm install`
4. `$ npm test`
5. `$ npm start`
6. The site should open in a new browser tab

## UI Description & Tasks
This is a fairly simple Create React App that is built using React 18 and MaterialUI. Prior knowledge of MaterialUI and React Testing Library is helpful but not required. If
you have never used a component library such as MaterialUI, Tailwind, AntDesign, etc. then it is recommended you take a look
at the [MaterialUI documentation](https://mui.com/) for some examples.

### Task 1
Fix the broken RTL tests.

### Task 2
The product owner wants to add an area in the application to display the current price of Bitcoin. Please read the user story
below and implement the 'Bitcoin' section as necessary to meet the **Acceptance Criteria**.

#### User Story
As a user, I should be able to see the 'Bitcoin' section laid out in a card format so that I can
easily consume 'Bitcoin Price'.

**Acceptance Criteria**
* Add a new nav item called 'Bitcoin'
* When clicked:
  * Hide the Chart, Recent Deposits, and Recent Orders components from the Dashboard
  * The title of the page should change from 'Dashboard' to 'Bitcoin Currency Exchange'
  * A new 'Bitcoin' component will show the latest currency data
* Retrieve the data from the API of CoinDesk located here: https://api.coindesk.com/v1/bpi/currentprice.json (use any HTTP client you like) 
* For each currency, use an Outlined Card from MaterialUI to display currency code, description and currency rate (truncate to 2 numbers after the decimal point)
* Display the Disclaimer at the bottom of the component
* Display the time the currencies were updated at the top of the component
* Write an RTL test to validate that your new component is being hydrated with data correctly
* Desktop: 3 cards per row
* Phone: 1 card per row

### Bonus Task
Create a component on the Dashboard that shows a table of Dog Facts from the completed API created in the PHP API task.

## Submission
After you have completed the tasks, please commit your local files and **push them up to your fork**. Notify the person
you have been working with that you have completed the test and provide them a link to **your** fork.

# Implematation comments

### DONE
- Create a Bitcoin Currency Exchange page and Bitcoin component to present the prices
- Install react-router and use it to create the routes to Dashboard and Bitcoin
- Install react-query and Axios to handle API calls and refresh data
- Fixed broken tests on the front and backend
- Duplicate CatFacts to DogFacts in the PHP project to hydrate the table in Dashboard
- Created the table in Dashboard to present DogFacts
- Install DataGrid from MUI to present the DogFacts and handle the pagination

### TODO
- Implement other routes
- Test the DogFact component
- Abstract header
- Contextually rename and reorder the listItems file that has the configPage
- Create subfolders to better organize the files by context and type (I keep without folders to maintain the pattern)
- Evaluate the need to change the Bitcoin component name to just Currency (to be reusable in case of need with another coin)
- Improve the design of the Bitcoin component to be more pretty :D