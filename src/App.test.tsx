import React from "react";
import { render, screen } from "@testing-library/react";
import Orders from "./Orders";
import Deposits from "./Deposits";
import Bitcoin from "./Bitcoin";

test("renders orders", () => {
  render(<Orders />);
  const title = screen.getByText(/recent orders/i);
  expect(title).toBeInTheDocument();
});

test("renders deposits", () => {
  render(<Deposits />);
  const title = screen.getByText(/recent deposits/i);
  expect(title).toBeInTheDocument();
});

test("renders bitcoin", () => {
  const test = {
    title: "Test",
    disclaimer: "Disclaimer test",
    updatedDate: "UpdatedDate test",
    code: "Code test",
    description: "Description test",
    rate: "13,125.15632",
  };

  render(
    <Bitcoin
      title={test.title}
      disclaimer={test.disclaimer}
      updatedDate={test.updatedDate}
      code={test.code}
      description={test.description}
      rate={test.rate}
    />
  );
  const title = screen.getByText(test.title);
  expect(title).toBeInTheDocument();
  const disclaimer = screen.getByText(test.disclaimer);
  expect(disclaimer).toBeInTheDocument();
  const updatedDate = screen.getByText(test.updatedDate);
  expect(updatedDate).toBeInTheDocument();
  const code = screen.getByText(test.code);
  expect(code).toBeInTheDocument();
  const description = screen.getByText(test.description);
  expect(description).toBeInTheDocument();
  const rate = screen.getByText("13,125.15");
  expect(rate).toBeInTheDocument();
});
