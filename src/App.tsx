import React from "react";
import "./App.css";
import AppRoutes from "./AppRoutes";
import { ThemeProvider, createTheme } from "@mui/material";
import BaseLayout from "./BaseLayout";

const mdTheme = createTheme();

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={mdTheme}>
        <BaseLayout>
          <AppRoutes />
        </BaseLayout>
      </ThemeProvider>
    </div>
  );
}

export default App;
