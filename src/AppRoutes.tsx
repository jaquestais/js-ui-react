import { Navigate, Route, Routes } from "react-router-dom";
import Dashboard from "./Dashboard";
import BitcoinCurrencyExchange from "./BitcoinCurrencyExchange";
import RoutePath from "./RoutePath";

export default function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<Navigate replace to={RoutePath.DASHBOARD} />} />
      <Route path={RoutePath.DASHBOARD} element={<Dashboard />} />
      <Route path={RoutePath.BITCOIN} element={<BitcoinCurrencyExchange />} />
    </Routes>
  );
}
