import Title from "./Title";
import { Card, CardContent, Typography } from "@mui/material";
import Currency from "./Currency";

const decimalLimitation = (value: string, places: number) => {
  const startDecimalPosition = value.lastIndexOf(".");
  return value.substring(0, startDecimalPosition + 1 + places);
};

interface BitcoinProps extends Currency {
  title?: string;
  disclaimer: string;
  updatedDate: string;
}

export default function Bitcoin({
  title = "Bitcoin",
  disclaimer,
  updatedDate,
  code,
  description,
  rate,
}: BitcoinProps): JSX.Element {
  return (
    <Card variant="outlined">
      <CardContent>
        <Typography sx={{ fontSize: 10 }} color="text.secondary">
          {updatedDate}
        </Typography>
        <Title>{title}</Title>
        <Typography>{code}</Typography>
        <Typography sx={{ fontSize: 12 }} color="text.secondary">
          {description}
        </Typography>
        <Typography>{decimalLimitation(rate, 2)}</Typography>
        <Typography sx={{ fontSize: 8 }} color="text.secondary">
          {disclaimer}
        </Typography>
      </CardContent>
    </Card>
  );
}
