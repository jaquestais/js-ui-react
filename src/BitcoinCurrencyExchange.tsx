import Grid from "@mui/material/Grid";
import Bitcoin from "./Bitcoin";
import { getBitcoinCurrentPrice } from "./fetchers";
import { useQuery } from "react-query";
import CurrencyExchange from "./CurrencyExchange";

export default function BitcoinCurrencyExchange() {
  const { data } = useQuery<CurrencyExchange>(
    "BitcoinCurrencyExchange",
    getBitcoinCurrentPrice
  );

  return (
    <Grid container spacing={3}>
      {data?.currencies &&
        Object.values(data.currencies).map((currency) => (
          <Grid item xs={12} md={4} key={currency.code}>
            <Bitcoin
              disclaimer={data?.disclaimer}
              updatedDate={data?.updatedDate}
              code={currency.code}
              description={currency.description}
              rate={currency.rate}
            />
          </Grid>
        ))}
    </Grid>
  );
}
