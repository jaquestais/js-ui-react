import Currency from "./Currency";

export default interface CurrencyExchange {
  chartName: string;
  disclaimer: string;
  updatedDate: string;
  currencies: { [key: string]: Currency };
}
