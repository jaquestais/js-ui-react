import * as React from "react";
import {
  DataGrid,
  GridColDef,
  GridValueFormatterParams,
} from "@mui/x-data-grid";
import Title from "./Title";
import { getDogFacts } from "./fetchers";
import { useQuery } from "react-query";
import DogFactsResponse from "./DogFactsResponse";
import { Box } from "@mui/material";

const columns: GridColDef[] = [
  { field: "id", headerName: "ID", width: 90 },
  {
    field: "fact",
    headerName: "Fact",
    flex: 1,
  },
  {
    field: "created_at",
    headerName: "Created At",
    flex: 1,
    valueFormatter: (params: GridValueFormatterParams<string>) => {
      return new Date(params.value).toDateString();
    },
  },
  {
    field: "updated_at",
    headerName: "Updated At",
    flex: 1,
    valueFormatter: (params: GridValueFormatterParams<string>) => {
      return new Date(params.value).toDateString();
    },
  },
];

export default function DogFacts() {
  const { data } = useQuery<DogFactsResponse>("DogFacts", getDogFacts);

  return (
    <React.Fragment>
      <Title>Dog Facts</Title>
      {data?.data && (
        <Box sx={{ height: 400, width: "100%" }}>
          <DataGrid
            rows={data.data}
            columns={columns}
            pageSize={5}
            rowsPerPageOptions={[5]}
            disableSelectionOnClick
          />
        </Box>
      )}
    </React.Fragment>
  );
}
