import axios from "axios";

const instanceCoindesk = axios.create({
  baseURL: "https://api.coindesk.com/v1/bpi",
});

const instanceAnimalFacts = axios.create({
  baseURL: "http://localhost:8042/api/v1",
});

export { instanceCoindesk, instanceAnimalFacts };
