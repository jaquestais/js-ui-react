import CurrencyExchange from "./CurrencyExchange";
import DogFactsResponse from "./DogFactsResponse";
import { instanceCoindesk, instanceAnimalFacts } from "./apis";

async function getBitcoinCurrentPrice(): Promise<CurrencyExchange> {
  const {
    data: { chartName, disclaimer, time, bpi },
  } = await instanceCoindesk.get("currentprice.json");

  return {
    chartName,
    disclaimer,
    updatedDate: time.updated,
    currencies: bpi,
  };
}

async function getDogFacts(): Promise<DogFactsResponse> {
  const { data } = await instanceAnimalFacts.get("dog-facts");

  return data;
}

export { getBitcoinCurrentPrice, getDogFacts };
