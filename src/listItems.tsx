import * as React from "react";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import DashboardIcon from "@mui/icons-material/Dashboard";
import CurrencyBitcoinIcon from "@mui/icons-material/CurrencyBitcoin";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PeopleIcon from "@mui/icons-material/People";
import BarChartIcon from "@mui/icons-material/BarChart";
import LayersIcon from "@mui/icons-material/Layers";
import AssignmentIcon from "@mui/icons-material/Assignment";
import RoutePath from "./RoutePath";
import { useNavigate } from "react-router-dom";

type ConfigPages = {
  [key in RoutePath]: {
    title?: string;
    label: string;
    path: string;
  };
};

export const configPages: ConfigPages = {
  [RoutePath.DASHBOARD]: {
    label: "Dashboard",
    path: RoutePath.DASHBOARD,
  },
  [RoutePath.BITCOIN]: {
    title: "Bitcoin Currency Exchange",
    label: "Bitcoin",
    path: RoutePath.BITCOIN,
  },
  [RoutePath.ORDERS]: {
    label: "Orders",
    path: RoutePath.DASHBOARD,
  },
  [RoutePath.CUSTOMERS]: {
    label: "Customers",
    path: RoutePath.DASHBOARD,
  },
  [RoutePath.REPORTS]: {
    label: "Reports",
    path: RoutePath.DASHBOARD,
  },
  [RoutePath.INTEGRATIONS]: {
    label: "Integrations",
    path: RoutePath.DASHBOARD,
  },
};

interface ListItemProps {
  children: React.ReactNode;
  path: RoutePath;
}

function ListItem({ children, path }: ListItemProps): JSX.Element {
  const navigate = useNavigate();
  const pagePath = configPages[path].path;

  return (
    <ListItemButton onClick={() => path && navigate(pagePath)}>
      <ListItemIcon>{children}</ListItemIcon>
      <ListItemText primary={configPages[path].label} />
    </ListItemButton>
  );
}

export const mainListItems = (
  <React.Fragment>
    <ListItem path={RoutePath.DASHBOARD}>
      <DashboardIcon />
    </ListItem>
    <ListItem path={RoutePath.BITCOIN}>
      <CurrencyBitcoinIcon />
    </ListItem>
    <ListItem path={RoutePath.ORDERS}>
      <ShoppingCartIcon />
    </ListItem>
    <ListItem path={RoutePath.CUSTOMERS}>
      <PeopleIcon />
    </ListItem>
    <ListItem path={RoutePath.REPORTS}>
      <BarChartIcon />
    </ListItem>
    <ListItem path={RoutePath.INTEGRATIONS}>
      <LayersIcon />
    </ListItem>
  </React.Fragment>
);

export const secondaryListItems = (
  <React.Fragment>
    <ListSubheader component="div" inset>
      Saved reports
    </ListSubheader>
    <ListItemButton>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Current month" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Last quarter" />
    </ListItemButton>
    <ListItemButton>
      <ListItemIcon>
        <AssignmentIcon />
      </ListItemIcon>
      <ListItemText primary="Year-end sale" />
    </ListItemButton>
  </React.Fragment>
);
